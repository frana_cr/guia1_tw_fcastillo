# Guia1_TW_Fcastillo
### Explicacion general.
En el siguiente laboratorio se desarrolla la construcción de una página web la cual contiene información de los 20 aminoácidos, en cada una de las paginas individuales para los aminoacidos se encontrara información básica sobre estos, una tabla resumen de sus abreviaciones, una imagen y gif de la estructura y a su vez un video de explicación junto con un link al sitio de wikipedia correspondiente a cada aminoácido. Tambien existe otra página en donde esta la biografía del autor del sitio.
Se utilizan imagenes, videos, links, tablas, gifs y listas junto con distintas herramientas de HTML y CSS.

### Desarrollo.
Para el correcto funcionamiento del sitio que se ha creado se generaron 22 archivos HTML y 1 CSS, cada uno de estos contiene informacion sobre las paginas.
    - De los 22 archivos HTML, existen 20 que son de cada aminoácido en donde se realiza la construcción de cada página junto con los elementos que son requeridos(videos, tablas, listas, menubar, links, gif). Tambien existe una pagina principal en donde se muestra el menubar para el acceso a cada página tambien se muestra una lista en forma de tabla para poder colocar de buena manera las imágenes de los aminoácidos con su nombre. Finalmente se ha creado un archivo que presenta la biografía del autor del sitio con información personal y con una imagen de este. 
    - El sitio a su vez cuenta con un estilo predeterminado en cada una de las páginas el cual resulta igual para todas en cuanto al formato de etiqueta, párrafos y otros aspectos. Esto se desarrollo en el archivo llamado estilo.css, en donde estan los parametros de diseño de cada uno de los archivos y de los elementos presentes. 
    - Existen distintas carpetas las cuales contienen imágenes que se han clasificado de manera ordenada para evitar la confusión de archivos:
        - estructuramolecular: esta carpeta contiene las imagenes de los esqueletos de cada uno de los aminoácidos.
        - estructuras3d: esta carpeta contendra las imagenes 3d de cada uno de los aminoácidos que estaran en el sitio inicial.
        - gifsAA: esta carpeta tendra los gifs de cada uno de los aminoácidos.


### Prerequisitos

- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (atom o vim)


### Ejecutando Pruebas
Para ejecutar el desarrollo de la guia1 se tiene que descargar cada uno de los archivos y hacer click en el archivo que se llama Index.html puesto que esta es la página principal del sitio que se ha creado y de esta manera luego se puede ir interactuando con las demás. Tambien existe un menubar en cada una de las páginas para poder volver al inicio de esta, a la página que contiene la biografía y finalmente a cada una de las páginas de los aminoácidos. 

      Construido con:
        - Ubuntu: Sistema operativo.
        - HTML: Lenguaje de programación.
        - Atom: Editor de código.


### Creación
Francisca Castillo (Desarrollo de código y edición README)
